module Model exposing
    ( LModel
    , Model
    , Msg(..)
    , init
    , ready
    , sinceCreated
    , subscriptions
    , update
    )

import Browser
import Browser.Events exposing (onAnimationFrame)
import Browser.Navigation as Nav
import Model.Game as Game exposing (Game)
import Task
import Time as Time exposing (Posix)
import Url as Url exposing (Url)


type LModel
    = Loading LoadingModel
    | Ready Model


type alias LoadingModel =
    { url : Url
    , nav : Nav.Key
    }


type alias Model =
    { created : Posix
    , reified : Posix
    , now : Posix
    , url : Url
    , nav : Nav.Key
    , mana : Int
    , souls : Int
    , soulFrags : Int
    , units : Game.Units
    , error : Maybe String
    }


type Msg
    = Init Posix
    | Tick Posix
    | UrlChange Url
    | UrlRequest Browser.UrlRequest
    | GameMsg Game.Msg


type alias Flags =
    ()


ready : LModel -> Maybe Model
ready lmodel =
    case lmodel of
        Loading _ ->
            Nothing

        Ready model ->
            Just model


sinceCreated : Model -> Int
sinceCreated model =
    Time.posixToMillis model.now - Time.posixToMillis model.created


init : Flags -> Url -> Nav.Key -> ( LModel, Cmd Msg )
init _ url nav =
    ( LoadingModel url nav |> Loading
    , Task.perform Init Time.now
    )


subscriptions : LModel -> Sub Msg
subscriptions _ =
    Sub.batch
        [ onAnimationFrame Tick
        ]


updateLoading : Msg -> LoadingModel -> LModel
updateLoading msg model =
    case msg of
        Init t ->
            { created = t
            , reified = t
            , now = t
            , url = model.url
            , nav = model.nav
            , mana = Game.init.mana
            , souls = Game.init.souls
            , soulFrags = Game.init.soulFrags
            , units = Game.init.units
            , error = Game.init.error
            }
                |> Ready

        UrlChange url ->
            { model | url = url } |> Loading

        _ ->
            model |> Loading


updateReady : Msg -> Model -> ( Model, Cmd Msg )
updateReady msg model =
    case msg of
        Tick t ->
            ( { model | now = t }, Cmd.none )

        UrlChange url ->
            ( { model | url = url }, Cmd.none )

        UrlRequest (Browser.Internal url) ->
            ( model, url |> Url.toString |> Nav.pushUrl model.nav )

        UrlRequest (Browser.External url) ->
            ( model, url |> Nav.load )

        Init t ->
            ( model, Cmd.none )

        GameMsg gm ->
            ( model |> Game.update gm, Cmd.none )


update : Msg -> LModel -> ( LModel, Cmd Msg )
update msg lmodel =
    case lmodel of
        Loading model ->
            ( updateLoading msg model, Cmd.none )

        Ready model ->
            updateReady msg model
                |> Tuple.mapFirst Ready
