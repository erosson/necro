module View.Orb exposing (mana, souls)

-- TODO upgrade the visual: http://www.2deegameart.com/2013/06/game-ui-design-mana-orb-inkscape-tutorial.html

import Svg as S exposing (..)
import Svg.Attributes as A exposing (..)


fullOrb : List (Attribute msg) -> Svg msg
fullOrb attrs =
    circle (attrs ++ [ cx "100", cy "100", r "100" ]) []


orbMask : String -> Float -> Svg msg
orbMask id pct =
    S.mask [ A.id id ]
        [ rect [ fill "white", x "0", y "0", width "100%", height "100%" ] []
        , rect [ fill "black", x "0", y "0", width "100%", height <| String.fromFloat (pct * 100) ++ "%" ] []
        ]


manaMask : Svg msg
manaMask =
    rect [] []


souls : { val : Int, max : Int } -> Svg msg
souls { val, max } =
    let
        maskId =
            "soulsMask"

        pct =
            toFloat (max - val) / toFloat max
    in
    g []
        [ orbMask maskId pct
        , fullOrb [ fill "red", A.mask <| "url(#" ++ maskId ++ ")" ]
        , fullOrb [ fill "transparent", stroke "black" ]
        ]


mana : { val : Int, max : Int } -> Svg msg
mana { val, max } =
    let
        maskId =
            "manaMask"

        pct =
            toFloat (max - val) / toFloat max
    in
    g []
        [ orbMask maskId pct
        , fullOrb [ fill "blue", A.mask <| "url(#" ++ maskId ++ ")" ]
        , fullOrb [ fill "transparent", stroke "black" ]
        ]
