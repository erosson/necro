module Model.Game exposing
    ( Game
    , Msg(..)
    , Unit(..)
    , Units
    , init
    , mana
    , manaUnreserved
    , nextSoulFrags
    , promote
    , soulFrags
    , souls
    , soulsUnreserved
    , totalManaCost
    , totalSoulCost
    , unitList
    , update
    )

import Array as Array exposing (Array)
import Dict as Dict exposing (Dict)
import Time as Time exposing (Posix)


type alias Game a =
    { a
        | created : Posix
        , reified : Posix
        , now : Posix
        , mana : Int
        , souls : Int
        , soulFrags : Int
        , units : Units
        , error : Maybe String
    }


type alias TieredUnit =
    Dict Tier Int


type alias RawUnits =
    { necromancer : TieredUnit
    , harvester : TieredUnit

    -- TODO: kinda weak early-game, but wildly overpowered as soon as you can afford a bunch of high-level ones.
    -- Increasing cost only delays that, though the timing of the transition from other units to enchanters is intersting.
    -- Design it as a game-ending unit? Or, apply it only to the current tier, not all lower tiers?
    , enchanter : TieredUnit
    }


type Units
    = Units RawUnits


type CalcUnits
    = CalcUnits RawUnits


type alias Tier =
    Int


type alias Index =
    Int


type Unit
    = Necromancer Tier
    | Harvester Tier
    | Enchanter Tier


maxTier : TieredUnit -> Maybe Tier
maxTier =
    Dict.keys >> List.maximum


manaCost : Unit -> Int
manaCost unit =
    case unit of
        Necromancer tier ->
            1 * 10 ^ tier

        Harvester tier ->
            1 * 10 ^ tier

        Enchanter tier ->
            1 * 100 ^ tier


tieredManaCost : (Tier -> Unit) -> TieredUnit -> Int
tieredManaCost unit =
    Dict.toList
        >> List.map
            -- (tier, count) -> (Unit, count) -> (costEach, count) -> cost
            (Tuple.mapFirst (unit >> manaCost)
                >> (\( costEach, count ) -> costEach * count)
            )
        >> List.sum


totalManaCost : Units -> Int
totalManaCost (Units units) =
    (units.necromancer |> tieredManaCost Necromancer)
        + (units.harvester |> tieredManaCost Harvester)
        + (units.enchanter |> tieredManaCost Enchanter)


totalSoulCost : Units -> Int
totalSoulCost (Units units) =
    [ units.necromancer, units.harvester, units.enchanter ]
        |> List.map Dict.values
        |> List.concat
        |> List.sum


calcUnits : Units -> CalcUnits
calcUnits (Units units) =
    let
        chants =
            calcTieredUnits Dict.empty 1 units.enchanter
    in
    CalcUnits
        { necromancer = calcTieredUnits chants 3 units.necromancer
        , harvester = calcTieredUnits chants 3 units.harvester
        , enchanter = chants
        }


calcTieredUnits : TieredUnit -> Int -> TieredUnit -> TieredUnit
calcTieredUnits spawnBonus spawnEach units =
    calcTieredUnits_ (maxTier units |> Maybe.withDefault 0) spawnBonus spawnEach units


calcTieredUnits_ : Tier -> TieredUnit -> Int -> TieredUnit -> TieredUnit
calcTieredUnits_ parentTier spawnBonus spawnEach0 units =
    if parentTier <= 0 then
        units

    else
        let
            parentCount =
                units |> Dict.get parentTier |> Maybe.withDefault 0

            childTier =
                parentTier - 1

            spawnEach =
                spawnEach0 + (spawnBonus |> Dict.get parentTier |> Maybe.withDefault 0)
        in
        units
            |> Dict.update childTier (Maybe.withDefault 0 >> (+) (parentCount * spawnEach) >> Just)
            |> calcTieredUnits_ childTier spawnBonus spawnEach


type alias Stats =
    { manaPerSec : Int, soulFragsPerSec : Int }


totalStats : CalcUnits -> Stats
totalStats (CalcUnits units) =
    { manaPerSec = units.necromancer |> Dict.get 0 |> Maybe.withDefault 0
    , soulFragsPerSec = units.harvester |> Dict.get 0 |> Maybe.withDefault 0
    }


sinceReifiedMs : Game a -> Int
sinceReifiedMs game =
    Time.posixToMillis game.now - Time.posixToMillis game.reified


statSinceReified : (Stats -> Float) -> Game a -> Int
statSinceReified statFn game =
    (game.units |> calcUnits |> totalStats |> statFn)
        * (game |> sinceReifiedMs |> toFloat)
        / 1000
        |> floor


manaSinceReified : Game a -> Int
manaSinceReified =
    statSinceReified (.manaPerSec >> toFloat)


soulFragsSinceReified : Game a -> Int
soulFragsSinceReified =
    statSinceReified (.soulFragsPerSec >> toFloat)


type alias Level =
    Int


type alias Exp =
    Int


levelUps : (Level -> Exp) -> ( Level, Exp ) -> ( Level, Exp )
levelUps nextExpFn (( level0, exp0 ) as levelexp0) =
    let
        nextExp =
            nextExpFn level0
    in
    if exp0 < nextExp then
        levelexp0

    else
        levelUps nextExpFn ( level0 + 1, exp0 - nextExp )


mana : Game a -> Int
mana game =
    game.mana + manaSinceReified game


soulLevels : Level -> Exp
soulLevels s =
    10 ^ (s - 3)


soulLevelExp : Game a -> ( Level, Exp )
soulLevelExp game =
    ( game.souls, game.soulFrags + soulFragsSinceReified game ) |> levelUps soulLevels


souls : Game a -> Int
souls =
    soulLevelExp >> Tuple.first


soulFrags : Game a -> Int
soulFrags =
    soulLevelExp >> Tuple.second


nextSoulFrags : Game a -> Int
nextSoulFrags =
    souls >> soulLevels


manaUnreserved : Game a -> Int
manaUnreserved game =
    mana game - totalManaCost game.units


soulsUnreserved : Game a -> Int
soulsUnreserved game =
    souls game - totalSoulCost game.units


reify : Game a -> Game a
reify game =
    { game
        | reified = game.now
        , mana = mana game
        , souls = souls game
        , soulFrags = soulFrags game
    }


init =
    { mana = 10
    , souls = 4
    , soulFrags = 0
    , units =
        Units
            { necromancer = Dict.empty
            , harvester = Dict.empty
            , enchanter = Dict.empty
            }
    , error = Nothing
    }


type Msg
    = Assign Unit
    | Promote Unit
    | Release Unit


rawUnitList : RawUnits -> List ( Unit, Int )
rawUnitList units =
    (units.necromancer |> Dict.toList |> List.map (Tuple.mapFirst Necromancer))
        ++ (units.harvester |> Dict.toList |> List.map (Tuple.mapFirst Harvester))
        ++ (units.enchanter |> Dict.toList |> List.map (Tuple.mapFirst Enchanter))


unitList : Units -> List ( Unit, Int )
unitList (Units units) =
    rawUnitList units


promote : Unit -> Unit
promote unit =
    case unit of
        Necromancer tier ->
            Necromancer (tier + 1)

        Harvester tier ->
            Harvester (tier + 1)

        Enchanter tier ->
            Enchanter (tier + 1)


validate : Game a -> Game a -> Game a
validate game0 game =
    if manaUnreserved game < 0 then
        { game0 | error = Just "Not enough mana" }

    else if soulsUnreserved game < 0 then
        { game0 | error = Just "Not enough souls" }

    else
        { game | error = Nothing }


assignUnit : Unit -> Int -> Units -> Units
assignUnit unit count (Units units) =
    Units <|
        case unit of
            Necromancer tier ->
                { units | necromancer = units.necromancer |> assignTier tier count }

            Harvester tier ->
                { units | harvester = units.harvester |> assignTier tier count }

            Enchanter tier ->
                { units | enchanter = units.enchanter |> assignTier tier count }


releaseUnit : Unit -> Int -> Units -> Units
releaseUnit unit count =
    assignUnit unit -count


assignTier : Tier -> Int -> TieredUnit -> TieredUnit
assignTier tier count =
    let
        updater count0 =
            case count0 |> Maybe.withDefault 0 |> (+) count of
                0 ->
                    Nothing

                c ->
                    Just c
    in
    Dict.update tier updater


update : Msg -> Game a -> Game a
update msg game =
    case msg of
        Assign unit ->
            game
                |> reify
                |> (\g -> { g | units = g.units |> assignUnit unit 1 })
                |> validate game

        Promote unit ->
            game
                |> reify
                |> (\g -> { g | units = g.units |> releaseUnit unit 1 |> assignUnit (promote unit) 1 })
                |> validate game

        Release unit ->
            game
                |> reify
                |> (\g -> { g | units = g.units |> releaseUnit unit 1 })
                |> validate game
