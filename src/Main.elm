module Main exposing (main)

import Browser exposing (application)
import Model as M
import View exposing (view)


main =
    application
        { init = M.init
        , onUrlChange = M.UrlChange
        , onUrlRequest = M.UrlRequest
        , update = M.update
        , subscriptions = M.subscriptions
        , view = view
        }
