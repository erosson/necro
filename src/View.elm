module View exposing (view)

import Browser exposing (Document)
import Dict as Dict exposing (Dict)
import Html as H exposing (Html)
import Html.Attributes as A
import Html.Events as E
import Model as M exposing (LModel, Model, Msg)
import Model.Game as G exposing (Game, Unit)
import Svg as S
import Svg.Attributes as SA
import View.Orb as Orb


view : LModel -> Document Msg
view model =
    { title = "hi?"
    , body = head ++ [ bodyLoading model ]
    }


head : List (Html msg)
head =
    [ H.node "link" [ A.rel "stylesheet", A.href "./main.css" ] []
    ]


bodyLoading : LModel -> Html Msg
bodyLoading lmodel =
    case lmodel |> M.ready of
        Nothing ->
            H.text "loading..."

        Just model ->
            body model


unitString : G.Unit -> String
unitString unit =
    case unit of
        G.Necromancer tier ->
            if tier == 0 then
                "Skeleton"

            else
                "Necromancer " ++ String.fromInt tier

        G.Harvester tier ->
            if tier == 0 then
                "Spectre"

            else
                "Harvester " ++ String.fromInt tier

        G.Enchanter tier ->
            if tier == 0 then
                "Enchanter"

            else
                "Enchanter " ++ String.fromInt tier


body : Model -> Html Msg
body model =
    let
        sinceCreated =
            M.sinceCreated model

        mana =
            G.mana model

        souls =
            G.souls model

        soulFrags =
            G.soulFrags model

        nextSoulFrags =
            G.nextSoulFrags model

        manaUnreserved =
            G.manaUnreserved model

        soulsUnreserved =
            G.soulsUnreserved model

        error =
            model.error |> Maybe.withDefault ""
    in
    H.div []
        [ H.div [] [ H.text <| "hello view " ++ String.fromInt sinceCreated ]
        , H.div [] [ H.text <| "Mana: " ++ String.fromInt manaUnreserved ++ "/" ++ String.fromInt mana ]
        , H.div []
            [ H.text <|
                "Souls: "
                    ++ String.fromInt soulsUnreserved
                    ++ "/"
                    ++ String.fromInt souls
                    ++ " + "
                    ++ String.fromInt soulFrags
                    ++ "/"
                    ++ String.fromInt nextSoulFrags
            ]
        , H.div [] [ H.text error ]
        , assignedUnits model.units
        , H.div [] [ assignUnitButton <| G.Necromancer 0 ]
        , H.div [] [ assignUnitButton <| G.Harvester 0 ]
        , H.div [] [ assignUnitButton <| G.Enchanter 0 ]
        , S.svg [ SA.height "205", SA.width "205" ] [ Orb.souls { val = soulsUnreserved, max = souls } ]
        , S.svg [ SA.height "205", SA.width "205" ] [ Orb.mana { val = manaUnreserved, max = mana } ]
        ]


assignedUnits : G.Units -> Html Msg
assignedUnits units =
    case units |> G.unitList of
        [] ->
            H.div [] [ H.text "No units assigned." ]

        list ->
            H.ul [] (list |> List.map assignedUnit |> List.map (H.li []))


assignedUnit : ( Unit, Int ) -> List (Html Msg)
assignedUnit ( unit, count ) =
    [ H.text <| unitString unit ++ " x" ++ String.fromInt count
    , promoteUnitButton unit
    , releaseUnitButton unit
    ]


assignUnitButton : Unit -> Html Msg
assignUnitButton unit =
    H.button [ E.onClick <| M.GameMsg <| G.Assign unit ] [ H.text <| "Assign " ++ unitString unit ]


promoteUnitButton : Unit -> Html Msg
promoteUnitButton unit =
    H.button [ E.onClick <| M.GameMsg <| G.Promote unit ] [ H.text <| "Promote to " ++ unitString (G.promote unit) ]


releaseUnitButton : Unit -> Html Msg
releaseUnitButton unit =
    H.button [ E.onClick <| M.GameMsg <| G.Release unit ] [ H.text <| "Release" ]
